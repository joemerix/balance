﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager_Script : MonoBehaviour
{
    public float timeToSpawn = 2f;
    public List<Transform> spawnPoints = new List<Transform>();
    public GameObject character_Prefab;
    public List<GameObject> character_Prefabs = new List<GameObject>();

    public Transform left_Waypoint;
    public Transform right_Waypoint;

    public Transform mouseInput;

    public bool gameOver;
    public GameObject gameOver_UI;

    void Start()
    {
        gameOver_UI.SetActive(false);
        InvokeRepeating("SpawnCharacter", timeToSpawn, timeToSpawn);
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
            if(hit.transform.tag == "Player")
                hit.transform.GetComponent<Character_Script>().selectable = true;

        if (gameOver)
            if (Input.GetKeyDown("space"))
                SceneManager.LoadScene("Game_Scene");
        if (Input.GetKeyDown("escape"))
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }

    void SpawnCharacter()
    {
        int characterSpawn_Int = Random.Range(0, character_Prefabs.Count);
        int spawn_Int = Random.Range(0, spawnPoints.Count);
        GameObject character_Clone = Instantiate(character_Prefabs[characterSpawn_Int], spawnPoints[spawn_Int].position, spawnPoints[spawn_Int].rotation);
        character_Clone.GetComponent<Character_Script>().left_Waypoint = left_Waypoint;
        character_Clone.GetComponent<Character_Script>().right_Waypoint = right_Waypoint;
        character_Clone.GetComponent<Character_Script>().mouseInput = mouseInput;
    }

    public void GameOver()
    {
        gameOver_UI.SetActive(true);
        gameOver = true;
    }
}