﻿using UnityEngine;

public class GameOverCollider_Script : MonoBehaviour
{
    public GameObject gameManager;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Finish")
        {
            gameManager.GetComponent<GameManager_Script>().GameOver();
            collision.gameObject.SetActive(false);
        }
    }
}
