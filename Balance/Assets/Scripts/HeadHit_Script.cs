﻿using UnityEngine;

public class HeadHit_Script : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.name == "Seesaw")
        {
            transform.parent.position = new Vector3(transform.position.x, transform.position.y, 10);
            transform.parent.GetComponent<Character_Script>().Fall();
        }
    }
}
