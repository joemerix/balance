﻿using UnityEngine;

public class Character_Script : MonoBehaviour
{
    [Header("Waypoints")]
    public Transform left_Waypoint;
    public Transform right_Waypoint;

    public Transform target_Waypoint;

    [Header("Numbers")]
    public float timeToDecision = 2f;
    public float movementSpeed = 2f;
    public float moveToInput_Speed = 6f;
    public float baseMass = 0.1f;
    public float currentMass;
    public float boostedMass = 0.4f;

    [Header("Selected")]
    public bool selected;
    public bool selectable;
    public Transform mouseInput;
    public bool falling;

    [Header("Audio")]
    public AudioClip moseyGrab;
    public AudioClip moseyDrop;
    public AudioSource audioSource;

    void Start()
    {
        Physics.IgnoreLayerCollision(8, 8);
        InvokeRepeating("WaypointDecision", timeToDecision, timeToDecision);
        Invoke("RandomWaypoint", 1f);
        currentMass = baseMass;
    }

    void Update()
    {
        if(target_Waypoint)
            transform.position = Vector3.MoveTowards(transform.position, target_Waypoint.position, movementSpeed);

        if (selectable)
            if (Input.GetMouseButtonDown(0))
            {
                audioSource.clip = moseyGrab;
                audioSource.Play(0);
                selected = true;
                falling = true;
            }
        if (selected)
            MoveTowardsInput();
        if (Input.GetMouseButtonUp(0))
        {
            selected = false;
            Invoke("RandomWaypoint", 1f);
        }
        selectable = false;
        if(target_Waypoint)
            if (transform.position == target_Waypoint.position)
                ChangeWaypoint();
        if (currentMass > baseMass)
            currentMass -= 0.05f * Time.deltaTime;
        //Set mass to currentMass;
    }

    void WaypointDecision()
    {
        float diceRoll = Random.Range(0f, 99f);
        if (diceRoll >= 49)
            ChangeWaypoint();
    }

    void ChangeWaypoint()
    {
        if (target_Waypoint == left_Waypoint)
            target_Waypoint = right_Waypoint;
        else if (target_Waypoint == right_Waypoint)
            target_Waypoint = left_Waypoint;
    }

    void RandomWaypoint()
    {
        float diceRoll = Random.Range(0, 99);
        if (diceRoll >= 49)
            target_Waypoint = left_Waypoint;
        else
            target_Waypoint = right_Waypoint;
    }

    void MoveTowardsInput()
    {
        target_Waypoint = null;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        transform.position = Vector3.MoveTowards(transform.position, mouseInput.position, moveToInput_Speed);
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }

    public void Fall()
    {
        CancelInvoke("WaypointDecision");
        target_Waypoint = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Seesaw")
            if (falling)
            {
                audioSource.clip = moseyDrop;
                audioSource.Play(0);
                falling = false;
            }
    }
}