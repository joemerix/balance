﻿using UnityEngine;

public class FollowMouse_Script : MonoBehaviour
{
    void Update()
    {
        Vector3 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position =  new Vector3 (mouseLocation.x, mouseLocation.y, 0);
    }
}